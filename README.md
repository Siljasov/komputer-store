# Komputer Store
Komputer Store is a simple website application that acts like a web store and bank. It's built using HTML, CSS and JavaScript.

## Structure
The website has 3 main components to it and all contribute to each other in some way.
### Bank 
The first is the bank. It's a very stripped down and simplified version of a bank that shows you your balance and allows you to get a loan. 
### Job
The second is the job. It allows you to work and get money, where you can then either transfer that money to your bank account, or decide to pay off your loan if you have one. 
### Store
The last is the store. The store has 2 parts to it. The first part allows you to select the laptop you want to purchase from a combo box. After a laptop is selected, its features will be shown here. The second part is the final section of the website, which allows the user to see more details about the laptop such as a picture, description and price. You'll be able to purchase the laptop here if you have the sufficient amount of money in your bank account.

## How to access
The website can be accessed in one of two ways. Either through visiting [this link here](https://komputer-store.herokuapp.com/) or building the repository locally and accessing it through `localhost`.  
