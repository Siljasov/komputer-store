class Laptop
{
    constructor(name, image, features, description, price)
    {
        this.name = name;
        this.image = image;
        this.features = features;
        this.description = description;
        this.price = price;
    }
}
const work = document.getElementById("pay-value");
const workButton = document.getElementById("work-button");
const bankButton = document.getElementById("bank-button");
const bankBalance = document.getElementById("bank-balance");
const loanButton = document.getElementById("loan-button");
const loanLabel = document.getElementById("loan-label");
const loanBalance = document.getElementById("loan-balance");
const loanInput = document.getElementById("loan-amount");
const submitLoanButton = document.getElementById("submit-loan");
const repayLoanButton = document.getElementById("repay-loan-button");
const laptopSelect = document.getElementById("laptop-select");
const laptopFeatures = document.getElementById("laptop-features");
const laptopImage = document.getElementById("laptop-image");
const laptopName = document.getElementById("laptop-name");
const laptopDescription = document.getElementById("laptop-description");
const laptopPrice = document.getElementById("laptop-price");
const buyLabel = document.getElementById("buy-message");
const buyButton = document.getElementById("buy-button");

let selectedLaptop = 0;
let laptops = [new Laptop("Lenovo Legion Y540", "./resources/laptop0.png", "Features: Intel Core i9-10850HK, 16GB DDR4 RAM, RTX 3060 6GB, 512GB SSD", "This sleek, portable 15.6-inch laptop pushes gaming performance to a new level. Its latest-generation specs guarantee you serious power. Thermally optimized to run cooler and quieter with a full-sized white-backlit keyboard, the Lenovo Legion Y540 Laptop is primed for those who demand gaming wherever life takes them.", 800),
    new Laptop("Razer Blade 15", "./resources/laptop1.png", "Features: Intel Core i7-10700H, 16GB DDR4 RAM, RTX 3070 6GB, 1024GB SSD", "The Razer Blade 15 with NVIDIA GeForce RTX 2070 with Max-Q Design graphics provides the ultimate PC gaming experience. Powered by an 8th Gen Intel Core i7 6 core processor and featuring a 15.6\" 144Hz Full HD display, housed in a precision crafted, compact CNC aluminum chassis just 0.70\" thin.", 1600),
    new Laptop("MacBook Pro 2021", "./resources/laptop2.png", "Features: Intel Core i9-10850HK, 32GB DDR4 RAM, AMD Radeon RX5700 8GB, 1024GB SSD", "A modest update over the 2019 model, the 2021 Pro does bring a much-needed new keyboard design, but it's not the 14-inch reimagining that many had expected. Still, it's a great workhorse that's the right balance of performance and longevity.", 2400),
    new Laptop("ASUS Strix Gaming X", "./resources/laptop3.png", "Features: AMD Ryzen 9 5900H, 16GB DDR4 RAM, RTX 3080 6GB, 256GB SSD", "The ROG Strix G15 embodies streamlined design, offering a formidable core experience for serious gaming and multitasking on Windows 10 Home. Featuring up to the latest 10th Gen Intel Core CPU and a NVIDIA GeForce GPU, it offers high-FPS power that takes full advantage of up to a blazing fast display", 1100)]

loanLabel.style.display = "none";
repayLoanButton.style.display = "none";
workButton.addEventListener('click', () => {
    let num = work.innerHTML;
    num = Number(num) + Number(100);
    work.innerHTML = num;
});

submitLoanButton.addEventListener('click', () => {
    if (Number(loanInput.value) <= Number(bankBalance.innerHTML) * 2)
    {
        loanLabel.style.display = "flex";
        loanBalance.innerHTML = loanInput.value;
        loanButton.style.display = "none";
        repayLoanButton.style.display = "inline-block";
        $('#loan-modal').modal('hide');
    }
    else console.log("Invalid loan amount");
})

bankButton.addEventListener('click', () =>
{
   let result = Number(work.innerHTML);
   if (Number(loanBalance.innerHTML) > 0)
   {
       const toBank = result * 0.90;
       const toLoan = result * 0.10;
       bankBalance.innerHTML = String(Number(toBank) + Number(bankBalance.innerHTML));
       loanBalance.innerHTML = String( Number(loanBalance.innerHTML) - Number(toLoan));
       if (Number(loanBalance.innerHTML) <= 0)
       {
           loanBalance.innerHTML = "0";
           loanLabel.style.display = "none";
           loanButton.style.display = "inline-block";
       }
       work.innerHTML = "0";
   }
   else
   {
       bankBalance.innerHTML = String(Number(result) + Number(bankBalance.innerHTML));
       work.innerHTML = "0";
   }
});

repayLoanButton.addEventListener('click', () => {
    if (Number(loanBalance.innerHTML) > 0 && Number(work.innerHTML) > 0)
    {
        loanBalance.innerHTML = String(Number(loanBalance.innerHTML) - Number(work.innerHTML));
        work.innerHTML = "0";
        if (Number(loanBalance.innerHTML) <= 0)
        {
            loanBalance.innerHTML = "0";
            loanLabel.style.display = "none";
            loanButton.style.display = "inline-block";
            repayLoanButton.style.display = "none";
        }
    }
})

laptopSelect.addEventListener('change', ()=>{
    selectedLaptop = Number(laptopSelect.value);
    laptopFeatures.innerHTML = laptops[selectedLaptop].features;
    laptopName.innerHTML = laptops[selectedLaptop].name;
    laptopImage.src = laptops[selectedLaptop].image;
    laptopDescription.innerHTML = laptops[selectedLaptop].description;
    laptopPrice.innerHTML = laptops[selectedLaptop].price;
});

buyButton.addEventListener('click', () =>{
    if (Number(bankBalance.innerHTML) >= laptops[selectedLaptop].price)
    {
        buyLabel.innerHTML = "You are now the owner of a new laptop!";
        bankBalance.innerHTML = String(Number(bankBalance.innerHTML) - Number(laptops[selectedLaptop].price));
    }
    else
    {
        buyLabel.innerHTML = "You don't have the funds to buy this laptop!";
    }
})